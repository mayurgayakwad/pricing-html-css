function changeSubscriptionPrice() {
    let button = document.querySelector(".togglebutton")
    let masterPrice = document.getElementById("masterPrice");
    let basePrice = document.getElementById("basePrice");
    let proPrice = document.getElementById("proPrice");

    if (button.checked == true) {
        masterPrice.textContent = "$39.99"
        basePrice.textContent = "$19.99"
        proPrice.textContent = "$24.99"
        
    } else {
        masterPrice.textContent = "$399.99"
        basePrice.textContent = "$199.99"
        proPrice.textContent = "$249.99"
    }
}